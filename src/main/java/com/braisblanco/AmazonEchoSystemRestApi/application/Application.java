package com.braisblanco.AmazonEchoSystemRestApi.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.braisblanco.AmazonEchoSystemRestApi.rest.RestHandler;

@SpringBootApplication
@ComponentScan(basePackageClasses=RestHandler.class)
public class Application {

	public static void main(String args[]) {
		
		SpringApplication.run(Application.class, args);
		
	}
	
}
