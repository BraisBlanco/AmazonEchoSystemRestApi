package com.braisblanco.AmazonEchoSystemRestApi.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Description;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.partitions.model.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.braisblanco.AmazonEchoSystemRestApi.data.ChatObject;
import com.braisblanco.AmazonEchoSystemRestApi.data.UserData;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RestServices {

	private static final String echoInboxQueueUrl = "";

	private static final String echoOutboxQueueUrl = "";

	private static final String bucket = "";

	private static final String ECHO_MESSAGE_TYPE = "echo";

	private static final String SEARCH_MESSAGE_TYPE = "search";
	
	private static String awsAccessKey;
	
	private static String awsSecretKey;
	
	private static String awsRegion;
	
	RestServices(@Value("${aws.access.key.id}") String accessKey,
						@Value("${aws.secret.key.id}") String secretKey,
						@Value("${aws.secret.key.id}") String region) {
		
		awsAccessKey = accessKey;
		awsSecretKey = secretKey;
		awsRegion = region;
		
	}

	public static UserData chat(UserData userData) {
		
		UserData dataToReturn = new UserData();

		AmazonSQS sqs = AmazonSQSClientBuilder.standard().
				withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		
		SendMessageRequest sendMessageReq = new SendMessageRequest();
		sendMessageReq.setQueueUrl(echoInboxQueueUrl);
		
		MessageAttributeValue attribute = new MessageAttributeValue();
		attribute.setStringValue(userData.getNickname());
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-id", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(userData.getSessionId());
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-sessionId", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(ECHO_MESSAGE_TYPE);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("message-type", attribute);
		
		String messageBody = userData.getMessage();
			
			if(messageBody.equalsIgnoreCase("END")) {

				return new UserData();
			}
			
		sendMessageReq.setMessageBody(messageBody);
		
		sqs.sendMessage(sendMessageReq);
		
		while(true) {
			
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(echoOutboxQueueUrl).withMessageAttributeNames("All").withMaxNumberOfMessages(1).withWaitTimeSeconds(5);	
			ReceiveMessageResult result = sqs.receiveMessage(receiveMessageRequest);
			
			List<Message> messages = result.getMessages();
			
			if(messages.isEmpty()) {
				continue;
			}
			
			if(!messages.get(0).getMessageAttributes().get("client-id").getStringValue().equals(userData.getNickname())) {
				System.out.println("There are messages, none of them are for me.\n");
				continue;
			}
				
			if(!messages.get(0).getMessageAttributes().get("client-sessionId").getStringValue().equals(userData.getSessionId())) {
				System.out.println("There are messages, none of them are for me.\n");
				continue;
			}
				
			dataToReturn.setMessage(messages.get(0).getBody());
			
			sqs.deleteMessage(new DeleteMessageRequest(echoOutboxQueueUrl, messages.get(0).getReceiptHandle()));
			
			break;
			
		}

		return dataToReturn;
	}
	
	public static UserData search(UserData userData) {
		
		UserData dataToReturn = new UserData();
		
		AmazonSQS sqs = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		
		SendMessageRequest sendMessageReq = new SendMessageRequest();
		sendMessageReq.setQueueUrl(echoInboxQueueUrl);
		
		MessageAttributeValue attribute = new MessageAttributeValue();
		attribute.setStringValue(userData.getNickname());
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-id", attribute);

		attribute = new MessageAttributeValue();
		attribute.setStringValue(userData.getSessionId());
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("client-sessionId", attribute);
		
		attribute = new MessageAttributeValue();
		attribute.setStringValue(SEARCH_MESSAGE_TYPE);
		attribute.setDataType("String");
		
		sendMessageReq.addMessageAttributesEntry("message-type", attribute);
		
		sendMessageReq.setMessageBody(userData.getMessage());
		
		sqs.sendMessage(sendMessageReq);
		
		while(true) {
			
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(echoOutboxQueueUrl).withMessageAttributeNames("All").withMaxNumberOfMessages(1).withWaitTimeSeconds(5);	
			ReceiveMessageResult result = sqs.receiveMessage(receiveMessageRequest);
		
			List<Message> messages = result.getMessages();
		
			if(messages.isEmpty()) {
				continue;
			}
		
			if(!messages.get(0).getMessageAttributes().get("client-id").getStringValue().equals(userData.getNickname())) {
				System.out.println("There are messages, none of them are for me.\n");
				continue;
			}
			
			if(!messages.get(0).getMessageAttributes().get("client-sessionId").getStringValue().equals(userData.getSessionId())) {
				System.out.println("There are messages, none of them are for me.\n");
				continue;
			}
		
			dataToReturn.setMessage(messages.get(0).getBody());
			
			sqs.deleteMessage(new DeleteMessageRequest(echoOutboxQueueUrl, messages.get(0).getReceiptHandle()));
		
			break;
			
		}
		
		return dataToReturn;
		
	}
	
	public static UserData download(UserData userData) throws JsonParseException, JsonMappingException, IOException{
		AmazonS3 s3 = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
				.withRegion(Regions.EU_WEST_3)
				.build();
		
		ArrayList<String> previousChatsKeys = new ArrayList<>();
		ArrayList<ChatObject> chatsRetrieved = new ArrayList<ChatObject>(); 
		UserData dataToReturn = new UserData();
		
		ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket).withPrefix(userData.getNickname().concat("/"));
		
		for (S3ObjectSummary s3ObSum : s3.listObjectsV2(req).getObjectSummaries()) {
			previousChatsKeys.add(s3ObSum.getKey());
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		for(String key: previousChatsKeys) {
		
			S3Object s3Obj = s3.getObject(bucket, key);
		  
			S3ObjectInputStream byteArray = s3Obj.getObjectContent();
		  
			chatsRetrieved.add(objectMapper.readValue(byteArray.readAllBytes(),ChatObject.class));

		}
		String messageToRet = "";
		for(ChatObject chat : chatsRetrieved) {
			messageToRet = messageToRet.concat("Session id " + chat.getSessionId() + ":\n");
			
			for(String message : chat.getChatMessages()) {
				messageToRet = messageToRet.concat(message + "\n");
			}
		}
		
		dataToReturn.setMessage(messageToRet);
		
		return dataToReturn;
	}

}
