package com.braisblanco.AmazonEchoSystemRestApi.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.braisblanco.AmazonEchoSystemRestApi.data.UserData;


@RestController
@CrossOrigin
public class RestHandler {
	
	@RequestMapping("/chat")
	public UserData chat(@RequestBody UserData userData) {
		return RestServices.chat(userData);
	}
	
	@RequestMapping("/search")
	public UserData doSearch(@RequestBody UserData userData) {
	
		return RestServices.search(userData);
	}
	
	@RequestMapping("/download")
	public UserData download(@RequestBody UserData userData) {
		try {
			return RestServices.download(userData);
		} catch (Exception e) {
			return new UserData();
		}
	}

}
