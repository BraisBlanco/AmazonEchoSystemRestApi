FROM openjdk:9-jre

COPY target/AmazonEchoSystemRestApi-0.1.0.jar app/AmazonEchoSystemRestApi-0.1.0.jar

EXPOSE 8080

CMD ["java", "-jar", "app/AmazonEchoSystemRestApi-0.1.0.jar"]
